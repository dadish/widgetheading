<?php

$h = $settings->heading;
$hasPages = (boolean) $renderPages->count();

if ($hasPages) {
  foreach ($renderPages as $p) {
    echo "<$h>$p->title</$h>";
  }
} else {
  echo "<$h>No page assigned for widget. Id: $widget->id</$h>";
}

?>